import { Component } from "@angular/core";
import { IMGS } from "./dummy-data";
import { FabricViewServiceService } from "./fabric-view/fabric-view-service.service";
import { layoutForImgNum } from "src/app/layouts-for-image-num";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  layout = "layout2";
  factor = 10;
  imgs: any[];

  constructor(private fabricView: FabricViewServiceService) {
    this.imgs = [];
    for (let index = 0; index < 9; index++) {
      this.imgs.push(IMGS[index] ? IMGS[index] : { id: null, img: null });
      this.imgs[index].style = this.assignGridArea(index);
      this.imgs[index].id = this.assignID();
    }
    console.log(this.imgs);
  }

  assignID() {
    return `ID${this.factor++}`;
  }

  assignGridArea(index: number) {
    return `grid-area: ${String.fromCharCode(97 + index)};`;
  }

  changeLayout(layout: string) {
    this.fabricView.change();
    this.layout = layout;
  }

  showItemInLayout(index: number): boolean {
    const layoutNum = parseInt(this.layout.split("t")[1], 10);
    const layoutMax = layoutForImgNum[layoutNum];
    return index < layoutMax;
  }
}
