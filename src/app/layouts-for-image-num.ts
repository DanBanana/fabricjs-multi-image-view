// The numbers in this array correspond with the max number of images for each layout.
// Index 1 represents 'Layout1' which takes in a maximum of 1 image.
// Index 2 represents 'Layout2' which takes in a maximum of 2 images.
// so on and so forth...

export const layoutForImgNum = [
  null, // null because having 0 images is illegal
  1, // Index 1
  2, // Index 2
  2,
  3,
  3,
  4,
  5,
  5,
  6,
];

// NOTE: This is strictly bound with 'src\app\layouts.scss'.
//       When adding/removing from any of these 2 files, the other
//       file should also be updated correctly.
