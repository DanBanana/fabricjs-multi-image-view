import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class FabricViewServiceService {
  $resetMultiView: Subject<any>;

  constructor() {
    this.$resetMultiView = new Subject();
  }

  change() {
    this.$resetMultiView.next();
  }
}
