import {
  Component,
  OnInit,
  ViewChild,
  Input,
  AfterViewInit,
  ElementRef,
  HostListener,
} from "@angular/core";
import { fabric } from "fabric";
import { FabricViewServiceService } from "./fabric-view-service.service";

@Component({
  selector: "app-fabric-view",
  templateUrl: "./fabric-view.component.html",
  styleUrls: ["./fabric-view.component.scss"],
})
export class FabricViewComponent implements OnInit, AfterViewInit {
  canvas: any;
  show = false;

  @Input("viewID") viewID: string;
  @Input("img") img: string;
  @ViewChild("wrapper") wrapper: ElementRef;
  constructor(private fabricView: FabricViewServiceService) {}

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.initCanvas();
    this.listenToChanges();
  }

  initCanvas() {
    setTimeout(() => {
      this.canvas = new fabric.Canvas(this.viewID, {
        width: this.holderW,
        height: this.holderH,
      });
      this.initImage();
      this.show = true;
    }, 10);
  }

  initImage() {
    fabric.Image.fromURL(this.img, (oImg: any) => {
      oImg.scaleToWidth(this.scale);
      this.canvas.backgroundColor = "white";
      this.canvas.add(oImg).centerObject(oImg);
    });
  }

  listenToChanges() {
    this.fabricView.$resetMultiView.subscribe(() => this.resizeCanvas());
  }

  @HostListener("window:resize")
  resizeCanvas() {
    try {
      this.show = false;
      this.canvas.clear();
      this.canvas.dispose();
      this.initCanvas();
    } catch {}
  }

  get holderW() {
    return this.wrapper ? this.wrapper.nativeElement.offsetWidth : null;
  }

  get holderH() {
    return this.wrapper ? this.wrapper.nativeElement.offsetHeight : null;
  }

  get scale() {
    const ret = this.holderH > this.holderW ? this.holderW : this.holderH;
    return ret - ret / 8;
  }
}
